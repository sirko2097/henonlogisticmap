package com.diploma.model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class LogisticMap {

    private List<Point> resultMap;
    private final List<Point> zeros = new ArrayList<>();

    public void logisticMap(double lambda, int amountOfIterations) {
        resultMap = new ArrayList<>();
        List<Double> xs = new ArrayList<>();

        double xn;
        double start = -1.0;
        double end = 1.0;
        double delta = 1.0 / amountOfIterations;

        xs.add(start);
        while (xs.get(xs.size() - 1) < end) {
            xn = xs.get(xs.size() - 1) + delta;
            xs.add(xn);
        }

        xs.forEach(x -> resultMap.add(new Point(x, 1 - lambda * x * x)));
    }

    public void logisticMapTree(int amountOfIterations) {
        resultMap = new ArrayList<>();
        double delta = 1.0 / amountOfIterations;

        for (double lambda = 0.5; lambda < 2; lambda += delta) {
            double x = 0.1;

            for (int t = 0; t < 100; t++) {
                x = 1 - lambda * x * x;
            }

            for (int t = 0; t < 20; t++) {
                x = 1 - lambda * x * x;
                resultMap.add(new Point(lambda, x));
            }
        }
    }

    public void lyapunovExponentLogisticMap(int amountOfIterations) {
        resultMap = new ArrayList<>();
        List<Double> result = new ArrayList<>();
        double x;
        double delta = 1.0 / amountOfIterations;

        for (double lambda = 0.5; lambda < 2.0; lambda += delta) {
            x = 0.01;
            result.clear();

            for (int t = 0; t < 100; t++) {
                x = 1 - lambda * x * x;

                result.add(Math.log(Math.abs(2 * lambda * x)));
            }

            zeros.add(new Point(lambda, 0.0));
            resultMap.add(new Point(lambda, (result.stream().mapToDouble(n -> n).sum()) / (result.size())));
        }
    }
}
