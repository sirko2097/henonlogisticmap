package com.diploma.model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class HenonMap {

    private List<Point> resultMap;
    private List<Point> zeros;


    public void henonMap(double a, double b, int amountOfIterations) {
        resultMap = new ArrayList<>();

        double xn = 0.0;
        double yn = 0.0;

        resultMap.add(new Point(xn, yn));

        for (int i = 0; i < amountOfIterations; i++) {
            xn = 1 - a * Math.pow(resultMap.get(i).getX(), 2) + resultMap.get(i).getY();
            yn = b * resultMap.get(i).getX();

            resultMap.add(new Point(xn, yn));
        }
    }

    public void henonMapTree(int amountOfIterations) {
        List<Point> alphas = new ArrayList<>();
        resultMap = new ArrayList<>();

        double xn = 0.00001;
        double yn = 0.00001;
        double a = 1.4;
        double b = 0.3;
        int i = 0;

        double delta = 1.0 / amountOfIterations;

        alphas.add(new Point(xn, yn));

        for (double alpha = 0.3; alpha < a; alpha += delta) {
            for (int j = 0; j < 100; j++) {
                xn = 1 - alpha * Math.pow(alphas.get(i).getX(), 2) + alphas.get(i).getY();
                yn = b * alphas.get(i).getX();

                alphas.add(new Point(xn, yn));
                i++;
            }

            i = 0;
            alphas.clear();
            alphas.add(new Point(xn, yn));

            for (int j = 0; j < 20; j++) {
                xn = 1 - alpha * Math.pow(alphas.get(i).getX(), 2) + alphas.get(i).getY();
                yn = b * alphas.get(i).getX();

                alphas.add(new Point(xn, yn));
                resultMap.add(new Point(alpha, xn));

                i++;
            }
        }
    }

    public void lyapunovExponentHenonMap(int amountOfIterations) {
        resultMap = new ArrayList<>();
        zeros = new ArrayList<>();

        double xn;
        double yn;
        List<Double> result = new ArrayList<>();
        List<Point> lambdas = new ArrayList<>();
        double a = 1.4;
        double b = 0.3;

        double delta = 1.0 / amountOfIterations;

        for (double alpha = 0.3; alpha < a; alpha += delta) {
            lambdas.clear();
            xn = 0.0;
            yn = 0.0;
            lambdas.add(new Point(xn, yn));
            result.clear();

            for (int t = 0; t < 1000; t++) {
                xn = 1 - alpha * Math.pow(lambdas.get(t).getX(), 2) + lambdas.get(t).getY();
                yn = b * lambdas.get(t).getX();

                result.add(Math.log(Math.abs(-2 * alpha * xn)));
                lambdas.add(new Point(xn, yn));
            }
            zeros.add(new Point(alpha, 0.0));
            resultMap.add(new Point(alpha, (result.stream().mapToDouble(n -> n).sum()) / (result.size())));
        }
    }
}
