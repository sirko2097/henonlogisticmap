package com.diploma.view;

import com.diploma.model.Point;
import lombok.Getter;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.stream.Collectors;

import static com.diploma.constants.Constants.LOGISTIC_MAP;
import static com.diploma.view.XYMapService.setupXyItemRenderer;

@Getter
public class AnimatedMappingChart extends JFrame {

    private XYSeries series;
    private XYSeries zeroLine;
    private final String titleForCheck;

    public AnimatedMappingChart(String title, List<Point> resultMap, List<Point> zeros, String xAxis, String yAxis,
                                boolean isLine) {
        super(title);
        this.titleForCheck = title;
        XYDataset dataset = createDataset(resultMap, zeros);

        // Create chart
        if (!isLine) {
            JFreeChart chart = ChartFactory.createScatterPlot(title, xAxis, yAxis, dataset);
            XYPlot plot = (XYPlot) chart.getPlot();
            plot.setBackgroundPaint(new Color(229, 225, 225, 225));


            XYItemRenderer renderer = setupXyItemRenderer(plot);
            plot.setRenderer(renderer);
            ChartPanel panel = new ChartPanel(chart);
            setContentPane(panel);
        } else {
            JFreeChart chart = ChartFactory.createXYLineChart(title, xAxis, yAxis, dataset);

            XYPlot plot = chart.getXYPlot();
            plot.setBackgroundPaint(new Color(229, 225, 225, 225));

            XYItemRenderer renderer = setupXyItemRenderer(plot);
            plot.setRenderer(renderer);
            ChartPanel panel = new ChartPanel(chart);
            setContentPane(panel);
        }
    }

    private XYDataset createDataset(List<Point> resultMap, List<Point> zeros) {
        XYSeriesCollection dataset = new XYSeriesCollection();

        final List<Double> xs = resultMap.stream()
                .map(Point::getX)
                .collect(Collectors.toList());

        final List<Double> ys = resultMap.stream()
                .map(Point::getY)
                .collect(Collectors.toList());

        series = new XYSeries("points");

        if (titleForCheck.contains(LOGISTIC_MAP)) {
            series.add(xs.get(0), ys.get(0));
            new Timer(1, new ActionListener() {
                private int i = 1;

                @Override
                public void actionPerformed(ActionEvent e) {
                    if (i < xs.size()) {
                        series.add(xs.get(i), ys.get(i));
                    }
                    i++;
                }

            }).start();

        } else {
            series.add(xs.get(100), ys.get(100));
            new Timer(1, new ActionListener() {
                private int i = 101;

                @Override
                public void actionPerformed(ActionEvent e) {
                    if (i < xs.size()) {
                        series.add(xs.get(i), ys.get(i));
                    }
                    i++;
                }

            }).start();

        }

        dataset.addSeries(series);

        if (!zeros.isEmpty()) {
            final List<Double> xZeros = zeros.stream()
                    .map(Point::getX)
                    .collect(Collectors.toList());

            final List<Double> yZeros = zeros.stream()
                    .map(Point::getY)
                    .collect(Collectors.toList());

            zeroLine = new XYSeries("zero");

            for (int i = 0; i < xZeros.size(); i++) {
                zeroLine.add(xZeros.get(i), yZeros.get(i));
            }

            dataset.addSeries(zeroLine);
        }

        return dataset;
    }
}
