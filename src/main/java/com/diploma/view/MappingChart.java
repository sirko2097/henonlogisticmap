package com.diploma.view;

import com.diploma.model.Point;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

import static com.diploma.constants.Constants.LOGISTIC_MAP;
import static com.diploma.view.XYMapService.setupXyItemRenderer;

public class MappingChart extends JFrame {

    private final String titleForCheck;

    public MappingChart(String title, List<Point> resultMap, List<Point> zeros, String xAxis, String yAxis,
                        boolean isLine) throws HeadlessException {

        super(title);
        this.titleForCheck = title;
        // Create dataset
        XYDataset dataset = createDataset(resultMap, zeros);

        // Create chart
        if (!isLine) {
            JFreeChart chart = ChartFactory.createScatterPlot(title, xAxis, yAxis, dataset);
            XYPlot plot = (XYPlot) chart.getPlot();
            plot.setBackgroundPaint(new Color(229, 225, 225, 225));


            XYItemRenderer renderer = setupXyItemRenderer(plot);
            plot.setRenderer(renderer);
            ChartPanel panel = new ChartPanel(chart);
            setContentPane(panel);
        } else {
            JFreeChart chart = ChartFactory.createXYLineChart(title, xAxis, yAxis, dataset);

            XYPlot plot = chart.getXYPlot();
            plot.setBackgroundPaint(new Color(229, 225, 225, 225));

            XYItemRenderer renderer = setupXyItemRenderer(plot);
            plot.setRenderer(renderer);
            ChartPanel panel = new ChartPanel(chart);
            setContentPane(panel);
        }
    }


    private XYDataset createDataset(List<Point> resultMap, List<Point> zeros) {
        XYSeriesCollection dataset = new XYSeriesCollection();

        List<Double> xs = resultMap.stream()
                .map(Point::getX)
                .collect(Collectors.toList());

        List<Double> ys = resultMap.stream()
                .map(Point::getY)
                .collect(Collectors.toList());

        XYSeries series = new XYSeries("points");

        if (titleForCheck.contains(LOGISTIC_MAP)) {
            for (int i = 0; i < xs.size(); i++) {
                series.add(xs.get(i), ys.get(i));
            }
        } else {
            for (int i = 100; i < xs.size(); i++) {
                series.add(xs.get(i), ys.get(i));
            }
        }

        dataset.addSeries(series);

        if (!zeros.isEmpty()) {
            xs = zeros.stream()
                    .map(Point::getX)
                    .collect(Collectors.toList());

            ys = zeros.stream()
                    .map(Point::getY)
                    .collect(Collectors.toList());

            XYSeries zeroLine = new XYSeries("zero");

            for (int i = 0; i < xs.size(); i++) {
                zeroLine.add(xs.get(i), ys.get(i));
            }

            dataset.addSeries(zeroLine);
        }

        return dataset;
    }
}
