package com.diploma.view;

import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class XYMapService {

    private XYMapService() {
    }

    public static XYItemRenderer setupXyItemRenderer(XYPlot plot) {
        XYItemRenderer renderer = plot.getRenderer();
        renderer.setSeriesPaint(0, Color.blue);
        double size = 1.0;
        double delta = size / 2.0;
        Shape shape = new Rectangle2D.Double(-delta, -delta, size, size);
        renderer.setSeriesShape(0, shape);
        return renderer;
    }
}
