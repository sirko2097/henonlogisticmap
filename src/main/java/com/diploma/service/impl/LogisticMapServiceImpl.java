package com.diploma.service.impl;

import com.diploma.model.LogisticMap;
import com.diploma.model.Point;
import com.diploma.service.DataProcess;
import com.diploma.service.LogisticMapService;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

import static com.diploma.constants.Constants.*;

@Service
public class LogisticMapServiceImpl implements LogisticMapService {

    private LogisticMap logisticMap = new LogisticMap();

    @Override
    public void logisticMap(double lambda, int amountOfIterations, boolean animate) {
        logisticMap.logisticMap(lambda, amountOfIterations);
        List<Point> resultMap = logisticMap.getResultMap();
        DataProcess.processData(LOGISTIC_MAP, resultMap, Collections.emptyList(), XN, XN_1, false, animate);
    }

    @Override
    public void logisticMapTree(int amountOfIterations, boolean animate) {
        logisticMap.logisticMapTree(amountOfIterations);
        List<Point> resultMap = logisticMap.getResultMap();
        DataProcess.processData(LOGISTIC_MAP_BIFURCATION_DIAGRAM, resultMap, Collections.emptyList(), LAMBDA, X, false, animate);
    }

    @Override
    public void lyapunovExponentLogisticMap(int amountOfIterations, boolean animate) {
        logisticMap.lyapunovExponentLogisticMap(amountOfIterations);
        List<Point> resultMap = logisticMap.getResultMap();
        List<Point> zeros = logisticMap.getZeros();
        DataProcess.processData(LOGISTIC_MAP_LYAPUNOV_EXPONENT, resultMap, zeros, LAMBDA, X, true, animate);
    }
}
