package com.diploma.service.impl;

import com.diploma.model.HenonMap;
import com.diploma.model.Point;
import com.diploma.service.DataProcess;
import com.diploma.service.HenonMapService;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

import static com.diploma.constants.Constants.*;

@Service
public class HenonMapServiceImpl implements HenonMapService {

    private final HenonMap henonMap = new HenonMap();

    @Override
    public void henonMap(double a, double b, int amountOfIterations, boolean animate) {
        henonMap.henonMap(a, b, amountOfIterations);
        List<Point> resultMap = henonMap.getResultMap();
        DataProcess.processData(HENON_MAP, resultMap, Collections.emptyList(), X, Y, false, animate);
    }

    @Override
    public void henonMapTree(int amountOfIterations, boolean animate) {
        henonMap.henonMapTree(amountOfIterations);
        List<Point> resultMap = henonMap.getResultMap();
        DataProcess.processData(HENON_MAP_BIFURCATION_DIAGRAM, resultMap, Collections.emptyList(), ALPHA, X, false, animate);
    }

    @Override
    public void lyapunovExponentHenonMap(int amountOfIterations, boolean animate) {
        henonMap.lyapunovExponentHenonMap(amountOfIterations);
        List<Point> resultMap = henonMap.getResultMap();
        List<Point> zeros = henonMap.getZeros();
        DataProcess.processData(HENON_MAP_LYAPUNOV_EXPONENT, resultMap, zeros, ALPHA, X, true, animate);
    }
}
