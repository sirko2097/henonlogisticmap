package com.diploma.service;

public interface HenonMapService {

    void henonMap(double a, double b, int amountOfIterations, boolean animate);

    void henonMapTree(int amountOfIterations, boolean animate);

    void lyapunovExponentHenonMap(int amountOfIterations, boolean animate);
}
