package com.diploma.service;

public interface LogisticMapService {

    void logisticMap(double lambda, int amountOfIterations, boolean animate);

    void logisticMapTree(int amountOfIterations, boolean animate);

    void lyapunovExponentLogisticMap(int amountOfIterations, boolean animate);
}
