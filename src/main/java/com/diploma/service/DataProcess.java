package com.diploma.service;

import com.diploma.model.Point;
import com.diploma.view.AnimatedMappingChart;
import com.diploma.view.MappingChart;

import javax.swing.*;
import java.util.List;

public final class DataProcess {

    private DataProcess() {
    }

    public static void processData(String tittle, List<Point> data, List<Point> zeros, String xAxisName, String yAxisName,
                                   boolean isLine, boolean animate) {
        if (!animate) {
            SwingUtilities.invokeLater(() -> {
                MappingChart mappingChart = new MappingChart(tittle, data, zeros, xAxisName, yAxisName, isLine);
                mappingChart.setSize(1280, 900);
                mappingChart.setLocationRelativeTo(null);
                mappingChart.setVisible(true);
            });
        } else {
            SwingUtilities.invokeLater(() -> {
                AnimatedMappingChart animatedMappingChart = new AnimatedMappingChart(tittle, data, zeros, xAxisName, yAxisName, isLine);
                animatedMappingChart.setSize(1280, 900);
                animatedMappingChart.setLocationRelativeTo(null);
                animatedMappingChart.setVisible(true);
            });
        }
    }
}
