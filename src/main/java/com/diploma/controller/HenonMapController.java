package com.diploma.controller;

import com.diploma.service.HenonMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/v1/henon-map")
public class HenonMapController {

    private final HenonMapService henonMapService;

    public HenonMapController(HenonMapService henonMapService) {
        this.henonMapService = henonMapService;
    }

    @PostMapping(value = "map")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity henonMap(@RequestParam double a, @RequestParam double b, @RequestParam int amountOfIterations,
                                   @RequestParam boolean animate) {
        henonMapService.henonMap(a, b, amountOfIterations, animate);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "tree")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity henonTree(@RequestParam int amountOfIterations, @RequestParam boolean animate) {
        henonMapService.henonMapTree(amountOfIterations, animate);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "lyapunov")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity henonLyapunovExponent(@RequestParam int amountOfIterations, @RequestParam boolean animate) {
        henonMapService.lyapunovExponentHenonMap(amountOfIterations, animate);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
