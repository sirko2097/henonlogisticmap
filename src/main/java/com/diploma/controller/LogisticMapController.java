package com.diploma.controller;

import com.diploma.service.LogisticMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/v1/logistic-map")
public class LogisticMapController {

    private final LogisticMapService logisticMapService;

    public LogisticMapController(LogisticMapService logisticMapService) {
        this.logisticMapService = logisticMapService;
    }

    @PostMapping(value = "map")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity logisticMap(@RequestParam double lambda, @RequestParam int amountOfIterations, @RequestParam boolean animate) {
        logisticMapService.logisticMap(lambda, amountOfIterations, animate);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "tree")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity logisticTree(@RequestParam int amountOfIterations, @RequestParam boolean animate) {
        logisticMapService.logisticMapTree(amountOfIterations, animate);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "lyapunov")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity logisticLyapunovExponent(@RequestParam int amountOfIterations, @RequestParam boolean animate) {
        logisticMapService.lyapunovExponentLogisticMap(amountOfIterations, animate);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
