package com.diploma.constants;

public class Constants {

    private Constants() {
    }

    public static final String LAMBDA = "lambda";
    public static final String X = "x";
    public static final String XN = "x[n]";
    public static final String XN_1 = "x[n+1]";
    public static final String Y = "y";
    public static final String ALPHA = "a";
    public static final String HENON_MAP = "Henon Map";
    public static final String HENON_MAP_BIFURCATION_DIAGRAM = "Phase-parametric characteristics of Henon Map";
    public static final String HENON_MAP_LYAPUNOV_EXPONENT = "Maximal of Lyapunov's characteristic exponents of Henon Map";
    public static final String LOGISTIC_MAP = "Logistic Map";
    public static final String LOGISTIC_MAP_BIFURCATION_DIAGRAM = "Phase-parametric characteristics of Logistic Map";
    public static final String LOGISTIC_MAP_LYAPUNOV_EXPONENT = "Maximal of Lyapunov's characteristic exponents of Logistic Map";
}
